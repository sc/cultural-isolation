# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 11:56:49 2019

@author: bscuser
"""


import pandas as pd
import numpy as np
import os
import glob
import matplotlib.pyplot as plt
from scipy.integrate import simps
import seaborn as sns



g_df = []
scenarios =[ 'freqpref','frequency', 'neutral', 'preference']

for scenario in scenarios:   

    path = "results/" + scenario + "*"
    for i in glob.glob(path): # reads all the files in the folder starting with the scenario name
        
        df = pd.read_csv(i, index_col = 0)   
        
        t_to_fix = df.shape[0] # record the time to fixation before you change the shape of the df
        
        ## propagate the last values until the max time is reached - normalises results over the same simulation duration 
        ## CHANGE 500 TO OTHER MAX-TIMES WHEN RUNNING LONGER SIMULATIONS
        add_df = pd.DataFrame(np.nan, index = np.arange(df.shape[0], 500), columns = df.columns)
        df = pd.concat([df, add_df]).ffill() 

        df["int1"], df["int2"], df["int3"] = df.apply(simps, axis = 0) # integrate to get the area under the curve using simpson's rule
        
        df["mean1"], df["mean2"], df["mean3"] = df.iloc[:, :3].apply(np.mean, axis = 0) # the mean value gives similar results
  
        df["scenario"] = scenario
        
        df["time"] = t_to_fix # time is a number of steps to fixation

        ## those two would have to be manually changed at every run 
        df["bias"] = 'conf'
        df["pref"] = str([0., 0.1, 0.1]).strip('[]')
        
        df = df.iloc[0, -10:] # only take one row (they're all the same)
        g_df.append(df)
        
df = pd.DataFrame(g_df) 
df.to_csv(scenario +'summary_results.csv')

### Additional visuals
grouped = df.groupby('scenario').mean().reset_index().set_index('scenario')
grouped.iloc[:, :3].plot( kind = 'bar')
plt.show()
grouped.time.plot(kind = 'bar')
plt.show()
plt.close()

# code for getting visuals of separate scenarios
pref = df[df.scenario == "preference"]
pref.head()
sns.boxplot(data = pref[['int1','int2', 'int3']])
for i in df.scenario.unique():
    snip = df[df.scenario == i] 
    sns.boxplot(data = snip[['int1','int2', 'int3']])
    plt.title(i)
    plt.show()
    
### For frequency and neutral we rank the results (i.e., most popular, middle, min)
freq = df[df.scenario == "frequency"][['int1','int2', 'int3']]
freq = freq * -1 # looks stupid but it workd for sorting
f_ranked = freq.apply(np.sort, axis = 1).apply(pd.Series)
f_ranked = f_ranked * -1 # and back to normal values
f_ranked.columns =['max','middle', 'min']

neut = df[df.scenario == "neutral"][['int1','int2', 'int3']]
neut = neut * -1
n_ranked = neut.apply(np.sort, axis = 1).apply(pd.Series)
n_ranked = n_ranked * -1
n_ranked.columns =['max','middle', 'min']


# visuals for ranked scenarios
for i in [f_ranked, n_ranked]:
    sns.boxplot(data = i[['max','middle', 'min']])
    plt.title(i)
    plt.show()

### The boxplot visuals
f, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize = [15, 5])

sns.boxplot(data = n_ranked[['max','middle', 'min']], ax = ax1)
ax1.set_title('neutral')

sns.boxplot(data = f_ranked[['max','middle', 'min']], ax = ax2)
ax2.set_title('frequency')

sns.boxplot(data = df[df.scenario == 'freqpref'][['int1','int2', 'int3']], ax = ax3)
ax3.set_title('intrVal + freq')

sns.boxplot(data = df[df.scenario == 'preference'][['int1','int2', 'int3']], ax = ax4)
ax4.set_title('intrinsic value')

plt.tight_layout()
plt.savefig("test.png", dpi = 600)
plt.show()
