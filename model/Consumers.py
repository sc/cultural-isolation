import random 
import string

#Definition of the Agent which are consumer in our case:
class Consumers(object):
    
    id=""
    all_products = {}
    
    
    def __init__(self, id, n_pots, all_products = None):
        """agent constructor"""
        
        alphabet = list(string.ascii_uppercase)

        if all_products == None:
            all_products = {}
        self.all_products=all_products
        
        self.id=id
        self.n_pots = n_pots
        
        ## give random initial preferences
        for i in range(self.n_pots):
            all_products[alphabet[i]]=random.randrange(0,2)     
    
    
    def __str__(self):
        """print an agent plus his preference list"""
        t = [ str(self.id) + ' has the following preferences:' ]
        for obj in self.all_products:
            s = '    ' + object.__str__(self.all_products[obj])
            t.append(s)
        return '\n'.join(t)  


    def mutate(self):
        """ mutate: randomly change one value in the preference list"""
        hit = random.choice(list(self.all_products.keys()))
        self.all_products[hit] = int(not(self.all_products[hit]))
    
    
    def copy_all(self,ag2):
        """copy all preferences of another agent. Not used"""
        self.all_products = ag2.all_products.copy()
     
        
    def copy_one(self,ag2, good, p_copy): 
        """copy one preference of another agent"""
        if random.random() < p_copy:
            self.all_products[good] = ag2.all_products[good]
     
        
    def drop_pots(self, no_drop_pots):
        """ drop required number of preferences"""
        ## find which indices have value 1 (you cannot drop a '0')
        changable = [ind for ind, val in self.all_products.items() if val == 1]
        ## choose randomly among them
        change_ind = random.sample(changable, no_drop_pots)
        ### change to 0
        for i in change_ind:
            self.all_products[i] = 0
    
