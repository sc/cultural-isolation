
from conformity import ConformitySimu
import pandas as pd
import sys
import os

if __name__ == "__main__":
    argv=sys.argv[1:]
    print(argv)
    #initialisation of the variables used during the simulation
    max_time = int(argv[0])      ## max length of simulation
    n_cons = int(argv[1])        ## number of consumers     
    p_mu = float(argv[2])      ## mutation probability 
    bias="conf"         ## "conf" or "anti" (conformist or anticonformist)
    p_copy = 1          ## probability of copy
    presence_bias=1     ## 1 - only copy existing preference (1s), 0 - copy option regardless if it's 1 or 0
    drop_pots = 0.5     ## 1 - if more prefrences than threshold remove at random, 0 - don't drop
    ### note drop_pots is a percentage that will be rounded down, eg., 50% or 3 options/goods is 1. 
    
    all_pref = [0.1, 0.1, 0.1]    ## inherent value of products, length == no. of options    
    n_pots = len(all_pref)  ## number of options 
    
    assert n_pots <= 26, "Don't make the no of pots higher than no of letters in latin alphabet - 26"

    delay = 1           ##  introduce additional option in due curse, 0.5 - halfway through the simulation; 1 - no delay
    scenarios = [ 'freqpref','frequency', 'neutral', 'preference']
    #scenarios = [ 'freqpref', 'preference']

    ###generate all parameters set 
    allparams=[]
    protoparam={
            "n_cons":n_cons, 
            "max_time":max_time, 
            "p_mu":p_mu, 
            "p_copy":p_copy,
            "bias":bias, 
            "n_pots":n_pots, 
            "all_pref":all_pref, 
            "i": "nosenar",
            "delay":delay, 
            "presence_bias":presence_bias,
            "drop_pots":drop_pots
            }
    for i in scenarios:
        if i in [ "freqpref","frequency"] : 
            for bias in ["anti", "conf"]:
                if i ==  "freqpref" : 
                    for p in [.105,.2,.8]:
                        for id in range(3):
                            allprefL=all_pref.copy()
                            allprefL[id]=p
                            newparam=protoparam.copy()
                            newparam["bias"]=bias
                            newparam["i"]=i
                            newparam["all_pref"]=allprefL
                            allparams.append(newparam)
                else:
                    newparam=protoparam.copy()
                    newparam["bias"]=bias
                    newparam["i"]=i
                    allparams.append(newparam)
        if i == "preference":
            for p in [.105,.2,.8]:
                for id in range(3):
                    allprefL=all_pref.copy()
                    allprefL[id]=p
                    newparam=protoparam.copy()
                    newparam["i"]=i
                    newparam["all_pref"]=allprefL
                    allparams.append(newparam)
        if i == "neutral":
            newparam=protoparam.copy()
            newparam["i"]=i
            allparams.append(newparam)
    #the use this set to run all simulations
    foldername="simulationoutput/"+"tstep="+argv[0]+"_n="+argv[1]+"_mu="+argv[2]
    if not os.path.exists(foldername):
        os.mkdir(foldername)
    
    for param in allparams:
        for run in range(100): # number of iterration of each run: 100, 1000
            main_exp=ConformitySimu(
                    param["n_cons"], 
                    param["max_time"], 
                    param["p_mu"], 
                    param["p_copy"],
                    param["bias"], 
                    param["n_pots"], 
                    param["all_pref"], 
                    param["i"], 
                    param["delay"], 
                    param["presence_bias"],
                    param["drop_pots"]
                    )
            allratio = main_exp.run(param["i"])
            expid=str(param["i"]) + param["bias"] +"_delay"+ str(param["delay"]) +"_pref" + "-".join(map(str,param["all_pref"])) + "_run" + str(run)
            path = foldername+'/'+expid+".csv"
            tmp=run
            while os.path.exists(path):
                tmp=tmp+1
                expid=str(param["i"]) + param["bias"] +"_delay"+ str(param["delay"]) +"_pref" + "-".join(map(str,param["all_pref"])) + "_run" + str(tmp)
                path = foldername+'/'+expid+".csv"

            # save results
            df = pd.DataFrame(allratio).fillna(0)
            df["scenario"]=param["i"]
            df["bias"]=param["bias"]
            df["local"]=param["all_pref"][0]
            df["regional"]=param["all_pref"][1]
            df["import"]=param["all_pref"][2]
            df.to_csv(path)
            print("done experiment:"+expid)

   
    """
    ### code used to do sensitivity analysis and testing
    for i in scenarios:
        
        combined_results = []
        for run in range(2):
            name = 'results/' + str(i) + bias +"_delay"+ str(delay) +"_pref" + str(all_pref).strip("[]") + "_run" + str(run)+'csv'
            main_exp=ConformitySimu(n_cons,max_time,p_mu,p_copy,bias, n_pots, all_pref, i, delay, presence_bias)

            allratio = main_exp.run(i, drop_pots)
            results = pd.DataFrame(allratio).fillna(0)
            #combined_results.append(allratio[-1]) ## if only want to see the end result
            combined_results.append(results)
            df = pd.DataFrame(combined_results)
            
            

        #df['mean'] = df.apply(np.mean, axis = 1)
        #df["mean"].hist()
        #plt.savefig(name +'1.png')
        #plt.close()
        #df.hist()
        #plt.savefig(name +'2.png')
        #plt.close()
        #print(i)
        #print(df.apply(np.mean, axis =0))
        
    """    
    """
    ### code used to run individual runs
    scenario = "frequency"
    main_exp=ConformitySimu(n_cons,max_time,p_mu,p_copy,bias, n_pots, all_pref, scenario, delay, presence_bias)
    allratio = main_exp.run(scenario, drop_pots)
    print(allratio[-1])
    results = pd.DataFrame(allratio).fillna(0) 
    
    
   # print("list of percentage of the good A:")
   # print(allratio)
    results.plot()
    
    plt.show()
    plt.close()
    results_perc = results.div(results.sum(axis=1), axis=0)
    #print(results_perc.head())
    results_perc.plot()
    plt.show()
    plt.close()
    #"""
