#!/usr/bin/python

import random 
import string
import numpy as np
from Consumers import Consumers #import the agent class
from operator import add

class ConformitySimu(object):
    """
    Definition of the object simulation that will manage the world and run the simulation

    """

    alphabet = list(string.ascii_uppercase)


    def __init__(self,n_cons,max_time,p_mu,p_copy,bias, n_pots, all_pref, scenario, delay, presence_bias, drop_pots):
        """
        Constructor
        """
        # number of agents
        self.n_cons=n_cons
        # stop condition
        self.max_time=max_time
        # probability of mutation
        self.p_mu=p_mu
        # probability of copying
        self.p_copy=p_copy
        # type of scenario (biased, antibiased, neutral)
        self.bias=bias
        # number of goods
        self.n_pots = n_pots
        # delay in introduction of a good; percentage of max_time
        self.delay = delay
        # alphabet to look up products 
        self.alphabet = list(string.ascii_uppercase)
        # array of inherent value of particular goods
        # if [1,1,1,1] then no preference for any good
        self.all_pref = all_pref
        # copy only present but not absent goods?
        self.presence_bias = presence_bias
        # if threshold reached remove goods at random
        self.drop_pots = drop_pots
        
        self.world = list() ## initialisation of the world
        
        self.init_agents()  ## initalisation of agents   
        # Sanity check for testing agents:
        # for i in range(n_pots):
        #    self.all_pref[self.alphabet[i]] = all_pref[i]
        #print("init ", self.all_pref)
        #print('Initialization of the world')



    def init_agents(self):
        """
        initialise the agents present in the world
        """
        for cons in range(self.n_cons):
            self.world.append(Consumers(cons, self.n_pots))
            
            
    def add_product(self):
        """
        give agents an additional product introduced over the course of the run
        """
        for cons in self.world :
            # each agent has a 20% chance of acquiring the new good
            if np.random.random_sample() < 0.20: 
                cons.all_products[self.alphabet[self.n_pots]] = 1
            else:
                cons.all_products[self.alphabet[self.n_pots]] = 0
        # update the world
        self.n_pots += 1 
        self.all_pref.append(1) # the 'inherent value' of the new product is set at maximum
        self.alltypes[-1].append(0)
        
        """
        # test to check that actually 20% of the consumers get the new product
        # should print out a number close to 0.20
        print(self.alltypes[-1])
        #rint('no pots, no pref', self.n_pots, self.all_pref)
        print ("consumer after: ", cons, cons.all_products)
      
        a = 0
        for i in range(len(self.world)):
            a = a + np.sum(self.world[i].all_products[alphabet[self.n_pots]] == 1)
        print(a / self.n_cons)
        """
        
    def run(self, scenario): 
        """ main function of the class Simulation => run a simulation """

##____________________world setup_________________________   
        ### take count of the number of each product in the population
        types_freq = [0]*self.n_pots
        
        for cons in self.world:
            agent = [cons.all_products[x] for x in cons.all_products]
            types_freq = list( map(add, agent, types_freq))

        self.alltypes = []
        self.alltypes.append(types_freq)
        
         
 ##______________________start of the simulation___________________ 
        for t in range(self.max_time):
            
            ### if all products are either on 0 or 100% of the population, no further change is possible and the simulation is stopped
            #if (set(self.alltypes[-1]).issubset( [0, self.n_cons] )): 
            #    break
            #
            ### Delayed introduction of one of the pots 
            ### NOTE the pot is introduced to 20% of the population (to change go to 'add product')           
            if t == int(self.max_time * self.delay): # delay is represented as a ratio between 0-1 where 1 is no delay
                self.add_product()        

            ### calculate the frequency of each product in the previous step              
            freq = [i / sum(self.alltypes[-1]) if sum(self.alltypes[-1]) != 0 else 0 for i in self.alltypes[-1]]
            ### initialise a list storing number of products in this step
            types_one_step=[0]*self.n_pots
            
            ##______________________Agents procedures___________________            
            
            for cons in self.world :
                
                # ____________ mutation ________ 
                if( random.random()< self.p_mu):
                    cons.mutate()
 
                else:
                   #____________ trade ________ 
                    ### randomly select another agent
                    n = random.randint(0,(self.n_cons-1)) # -1 cause indexing from 0 in the next line
                    cons2 = self.world[n]
        
                    # ensure trading partner is not me
                    while cons.id == cons2.id:
                        n=random.randint(0,(self.n_cons-1))
                        cons2 = self.world[n]
                        
                    # access partners preferences    
                    cons2types = [cons2.all_products[x] for x in cons2.all_products]
            ### _________________PROBABILITY MATRIX____________________________
                    ### ____________neutral_____________
                    if scenario == "neutral":
                    # equal probability for each scenario [1, 1, 1, 1]
                        prob_matrix = [1 for i in cons2types]
                    
                    #   print(scenario)
                    if scenario == "frequency":
                        #probability proportional to frequency of each good in the population
                         
                        if self.bias == "anti":
                            # the more common, the less probability prob = 1-frequency
                            prob_matrix = [1 - i for i in freq]
                        else:
                            # the more common, the more probability prob = freq
                            prob_matrix = freq
                   
                    ### ____________preference - inherent value_____________
                    if scenario == "preference":
                        # probability equal to inherent value
                        prob_matrix = self.all_pref
                  
                     
                    ### ____________combination_____________    
                    if scenario == "freqpref":
                        # probability is a sum of frequency and preference
                        if self.bias == "anti":
                            prob_matrix = [1 - i for i in freq]
                            prob_matrix = np.add(self.all_pref, prob_matrix)
                        else:
                            prob_matrix = np.add(self.all_pref, freq)
                    
                   
                    #a python bug: if all values in prob_matrix are 0 it chooses the last element of the sequence
                    assert not all(i == 0 for i in prob_matrix), "All options have 0 probability"
                    
                    ### ____________roulette wheel - choose what to copy_____________   
                    good = random.choices([*cons2.all_products], prob_matrix)[0]
                    
                    ### ____________copy____________
                    if self.presence_bias:
                        ### presence bias means only existing preferences are copied
                        if cons2.all_products[good] != 0:
                            cons.copy_one(cons2, good, self.p_copy)
                    else: 
                        ### no presence bias means both 1s and 0s are copied
                        cons.copy_one(cons2, good, self.p_copy)
                               
                    ### ____________drop____________
                    if self.drop_pots != 0: 
                        # if threshold (drop_pots parameter) is passed remove pots at random until no of preferences is reduced to the threshold
                        cons_pots = [cons.all_products[x] for x in cons.all_products]
                       
                        if sum(cons_pots) > int(len(cons.all_products) * self.drop_pots):
                            no_redundant_pots = sum(cons_pots) - int(len(cons.all_products) * self.drop_pots)
                            cons.drop_pots(no_redundant_pots)

                ### ____________add the agent to the count for this timestep____________
                agent = [cons.all_products[x] for x in cons.all_products]
                types_one_step = list( map(add, agent, types_one_step))
            
            ### ____________record the final count of this timestep____________    
            self.alltypes.append(types_one_step)
   
            
  
        return self.alltypes
        



