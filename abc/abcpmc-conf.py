from sampler import *
from abcpmc import LinearEps
import numpy as np
import conformity
import sys,os

### if use with MPI
#from abcpmc import mpi_util

pref=sys.argv[1] #a prefix that will be used as a folder to store the result of the ABC
bias=pref

if not os.path.exists(pref):
        os.makedirs(pref)

#distance function: sum of abs mean differences, we take Y (the evidenceS) as a list of percentage of the same sie than x but with value .95. _ie_ our evidence are a theoretical case where all the goods are at 95% of the same type during all the years
def dist(x, y):
    y=np.empty(len(x))
    y.fill(0.95)
    return np.mean(np.abs(np.mean(x, axis=0) - np.mean(y, axis=0)))

#our "model", a gaussian with varying means
def postfn(theta):
    # we reject the particul with no credible parameters (ie pop < 0 etc...)
    if(theta[0]<=1 or theta[1]<=0 or theta[1]>1 or theta[2]<=0 or theta[2]>1):
        return([-10000])

    else:
        ## we fixed the number of time step and we look only at three parameter: posize copy and mutation
        exp=conformity.ConformitySimu(int(theta[0]),80,theta[1],theta[2],bias)
        return exp.run()

data=""  #we dont use it in this expe

eps = LinearEps(5, 1, 0.115)
prior = TophatPrior([10,0,0],[20,1,1])

### if used with MPI
#mpi_pool = mpi_util.MpiPool()
#sampler = abcpmc.Sampler(N=100, Y=data, postfn=postfn, dist=dist,pool=mpi_pool) 
###

sampler = Sampler(N=200, Y=data, postfn=postfn, dist=dist)

for pool in sampler.sample(prior, eps):
    print("T:{0},eps:{1:>.4f},ratio:{2:>.4f}".format(pool.t, pool.eps, pool.ratio))
    for i, (mean, std) in enumerate(zip(np.mean(pool.thetas, axis=0), np.std(pool.thetas, axis=0))):
        print(u"    theta[{0}]: {1:>.4f} \u00B1 {2:>.4f}".format(i, mean,std))

    np.savetxt(bias+"/result_"+str(pool.eps)+".csv", pool.thetas, delimiter=",",fmt='%1.5f',comments="")
    #np.savetxt(bias+"/result_"+str(pool.eps)+".csv", pool.thetas, delimiter=",",header="n_agents,time,p_mu,p_copy",fmt='%1.5f',comments="")

